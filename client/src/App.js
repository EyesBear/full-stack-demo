import React, { Component } from 'react';
import './App.css';
import Table, { TableBody, TableCell, TableHead, TableRow } from 'material-ui/Table';
import TextField from 'material-ui/TextField';

const URL = 'https://experimentation.getsnaptravel.com/interview/hotels';

const getHotelPrices = async (postData, provider) => {
  postData.provider = provider;
  const response = await fetch(
    URL,
    {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify(postData),
    });
  return await response.json();
}

const getMatchedHotels = (retailHotels, stHotels) => {
  const retailHotelsMap = {};
  const matchedHotelsList = [];

  retailHotels.forEach(hotel => {
    retailHotelsMap[hotel.id] = hotel;
  })

  stHotels.forEach(hotel => {
    let id = hotel.id;
    if (id in retailHotelsMap){
      hotel.retailPrice = retailHotelsMap[id].price;
      matchedHotelsList.push(hotel);
    }
  })

  return matchedHotelsList;
}
class App extends Component {
  constructor() {
    super();
    this.state = {
      city: 'toronto',
      checkin: '2018-03-29',
      checkout: '2018-03-30',
      provider: undefined,
      hotelPrices: []
    }
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const responses = await Promise.all(
        [getHotelPrices(this.state, 'retail'), getHotelPrices(this.state, 'snaptravel')]
      );
      this.setState({
        hotelPrices: getMatchedHotels(responses[0].hotels, responses[1].hotels)
      });  
    } catch (error) {
      // TODO: handle the error
      console.log(error);
    }
  };

  render() {
    return (
      <div className="App">
        <form onSubmit={this.handleSubmit}>
          <TextField
            label="City"
            value={this.state.city}
            onChange={this.handleChange('city')}
            margin="normal"
          />
          <TextField
            label="Check-in"
            value={this.state.checkin}
            onChange={this.handleChange('checkin')}
            margin="normal"
          />
          <TextField
            label="Check-out"
            value={this.state.checkout}
            onChange={this.handleChange('checkout')}
            margin="normal"
          />
          <button>Search</button>
        </form>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Hotel Name</TableCell>
              <TableCell numeric>Number of Reviews</TableCell>
              <TableCell>Address</TableCell>
              <TableCell numeric>Number of Stars</TableCell>
              <TableCell>Amenities</TableCell>
              <TableCell>Image</TableCell>
              <TableCell numeric>SnapTravel Price</TableCell>
              <TableCell numeric>Hotel.com Price</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.state.hotelPrices.map(hotel => {
              return (
                <TableRow key={hotel.id}>
                  <TableCell>{hotel.id}</TableCell>
                  <TableCell>{hotel.hotel_name}</TableCell>
                  <TableCell numeric>{hotel.num_reviews}</TableCell>
                  <TableCell>{hotel.address}</TableCell>
                  <TableCell numeric>{hotel.num_stars}</TableCell>
                  <TableCell>{hotel.amenities.join(' ')}</TableCell>
                  <TableCell><img width="350px" alt="" src={hotel.image_url} /></TableCell>
                  <TableCell numeric>{hotel.price}</TableCell>
                  <TableCell numeric>{hotel.retailPrice}</TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </div>
    );
  }
}

export default App;
