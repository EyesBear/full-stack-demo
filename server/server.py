from flask import Flask, request, jsonify
app = Flask(__name__)
import requests, json

URL = 'https://experimentation.getsnaptravel.com/interview/hotels'
HEADERS = {'content-type': 'application/json'}

@app.route('/', methods=["POST"])
def hello_world():
    data = json.loads(request.data)

    stData =  _get_prices(data, 'snaptravel')
    retailData = _get_prices(data, 'retail')
    
    matchedData = _get_matched_data(stData, retailData)

    return jsonify(matchedData)

def _get_prices(data, provider):
    data['provider'] = provider
    res = requests.post(URL, data=request.data, headers=HEADERS)
    return json.loads(res.content)['hotels']

def _get_matched_data(stData, retailData):
    retailHotelsMap = {hotel['id']: hotel for hotel in retailData}
    matchedHotelsList = []
    for hotel in stData:
        if hotel['id'] in retailHotelsMap:
            hotel['retailPrice'] = retailHotelsMap[hotel['id']]
            matchedHotelsList.append(hotel)
    return matchedHotelsList
